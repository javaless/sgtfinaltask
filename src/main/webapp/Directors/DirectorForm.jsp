<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="Images/Group 2.png">
<style>
<%@ include file="../style.css" %>
</style>


<title>Directors</title>
</head>
<body>
<div class="header">
	<h1>
	    <c:if test="${director != null}">
	        Edit Director
	    </c:if>
	    <c:if test="${director == null}">
	        Add New Director
	    </c:if>
	</h1>
</div>
  <%@ include file="../navbar.html" %>


  <div class="main">
        <c:if test="${director != null}">
            <form action="update" method="post">
        </c:if>
        <c:if test="${director == null}">
            <form action="insert" method="post">
        </c:if>
      <c:if test="${director != null}">
          <input type="hidden" name="id" value="<c:out value='${director.getId()}' />" />
      </c:if>    
	  <label for="name">Director name:</label><br><input type="text" id="name" name="name" size="100" value="<c:out value='${director.getName()}' />" ><br><br>
	  <label for="surname">Director surname:</label><br><input type="text" id="surname" name="surname" size="100" value="<c:out value='${director.getSurname()}' />" ><br><br>
	  	<c:if test="${director != null}">
            <input type="submit" value="Update director" > 
        </c:if>
        <c:if test="${director == null}">
            <input type="submit" value="Add director" > 
        </c:if>
	  
	  
	  <a href="list">Cancel</a>
	  
  	</form>
  </div>
  
</body>
</html>