<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="Images/Group 2.png">
<style>
<%@ include file="../style.css" %>
</style>


<title>films</title>
</head>
<body>
<div class="header">
        <h1 id="artists" align="center"> Directors </h1>
  </div>
  <%@ include file="../navbar.html" %>


  <div class="main">
    <h2>List of Directors</h2>
    <p>You could add, rename and delete directors here.</p>
    <a href="/SGTfinalTask/Directors/new">Add new director</a>
  </div>
    <div align="center">
        <table border="1">
            <caption><h2>List of Directors</h2></caption>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="director" items="${directors}">
                <tr>
                    <td><c:out value="${director.getId()}" /></td>
                    <td><c:out value="${director.getName()}" /></td>
                    <td><c:out value="${director.getSurname()}" /></td>
                    <td>
                        <a href="/SGTfinalTask/Directors/edit?id=<c:out value='${director.getId()}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/SGTfinalTask/Directors/delete?id=<c:out value='${director.getId()}' />">Delete</a>                     
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div> 
    <p style="color:​red;"><c:out value='${validationError}' /></p>
  
</body>
</html>