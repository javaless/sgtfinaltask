<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="Images/Group 2.png">
<style>
<%@ include file="../style.css" %>
</style>


<title>Films</title>
</head>
<body>
<div class="header">
	<h1>
	    <c:if test="${film != null}">
	        Edit Film
	    </c:if>
	    <c:if test="${film == null}">
	        Add New Film
	    </c:if>
	</h1>
</div>
  <%@ include file="../navbar.html" %>

  
  <div class="main">
  <c:if test="${directors.size() > 0}">
        <c:if test="${film != null}">
            <form action="update" method="post">
        </c:if>
        <c:if test="${film == null}">
            <form action="insert" method="post">
        </c:if>
      <c:if test="${film != null}">
          <input type="hidden" name="id" value="<c:out value='${film.getId()}' />" />
      </c:if>    
	  <label for="title">Film title:</label><br><input type="text" id="title" name="title" size="100" value="<c:out value='${film.getTitle()}' />" ><br><br>
	  <label for="director_id">Film director:</label><br>
	  
	  <select id="director_id" name="director_id" width="100">
  	    <c:forEach var="director" items="${directors}">
  	    	<option value="<c:out value="${director.getId()}" />"><c:out value="${director.getFullName()}" /></option>
           </c:forEach>
	  </select><br><br>
	  <label for="title">Film year:</label><br><input type="text" id="year" name="year" size="100" value="<c:out value='${film.getYear()}' />" ><br><br>
	  
	  <br><br>
	  <c:if test="${film != null}">
            <input type="submit" value="Update film" > 
        </c:if>
        <c:if test="${film == null}">
            <input type="submit" value="Add film" > 
        </c:if>
	  
	  
	  <a href="list">Cancel</a>
	  
  	</form>
  	</c:if>
  	<c:if test="${directors.size() == 0}">
  		Please create directors before creating films
  	</c:if>
  </div>
  
  
</body>
</html>