<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="Images/Group 2.png">
<style>
<%@ include file="../style.css" %>
</style>


<title>Films</title>
</head>
<body>
<div class="header">
	<h1>
	        Manage film actors
	</h1>
</div>
  <%@ include file="../navbar.html" %>


  <div class="main">
		<br><br>
	  <label for="director_id">Title:</label>
	  	<c:out value='${film.getTitle()}' />
	  	<br><br>
	  <label for="director_id">Director:</label>
	  	<c:out value='${film.getDirector().getFullName()}' />
	  	<br><br>
	  <label for="director_id">Year:</label>
	  	<c:out value='${film.getYear()}' />
	  	<br><br>
	  <c:if test="${availableActors.size() > 0}">
	  <form action="addactor"  method="post">
	 	 <input type="hidden" name="id" value="<c:out value='${film.getId()}' />" />
	 	 <label for="director_id">Add another actor:</label>
		  <select id="actor_id" name="actor_id" width="100">
	  	    <c:forEach var="actor" items="${availableActors}">
	  	    	<option value="<c:out value="${actor.getId()}" />"><c:out value="${actor.getFullName()}" /></option>
	           </c:forEach>
		  </select><br><br>
		  <input type="submit" value="Add actor" > 
	  	</form>
	  </c:if>
	  	
	  	<h2>Film actors</h2><br>
        <table border="1">
            <tr>
                <th>Full name</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="actor" items="${film.getActors()}">
                <tr>
                    <td><c:out value="${actor.getFullName()}" /></td>
                    <td>
                        <a href="/SGTfinalTask/Films/removeactor?id=<c:out value='${film.getId()}' />&actor_id=<c:out value='${actor.getId()}' />">Remove</a>                 
                    </td>
                </tr>
            </c:forEach>
        </table>
  </div>
  
</body>
</html>