<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="Images/Group 2.png">
<style>
<%@ include file="../style.css" %>
</style>


<title>films</title>
</head>
<body>
<div class="header">
        <h1 id="artists" align="center"> Films </h1>
  </div>
  <%@ include file="../navbar.html" %>


  <div class="main">
    <h2>List of Films</h2>
    <p>You could add, rename and delete films here.</p>
    <a href="/SGTfinalTask/Films/new">Add new film</a>
  </div>
    <div align="center">
        <table border="1">
            <caption><h2>List of Films</h2></caption>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Director</th>
                <th>Year</th>
                <th>Actors</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="film" items="${films}">
                <tr>
                    <td><c:out value="${film.getId()}" /></td>
                    <td><c:out value="${film.getTitle()}" /></td>
                    <td><c:out value="${film.getDirector().getFullName()}" /></td>
                    <td><c:out value="${film.getYear()}" /></td>
                    <td><c:out value="${film.getActorNames()}" /></td>
                    <td>
                        <a href="/SGTfinalTask/Films/edit?id=<c:out value='${film.getId()}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/SGTfinalTask/Films/actors?id=<c:out value='${film.getId()}' />">Manage actors</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/SGTfinalTask/Films/delete?id=<c:out value='${film.getId()}' />">Delete</a>                  
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div> 
  
</body>
</html>