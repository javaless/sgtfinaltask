package pages;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Actor;
import model.ActorDAO;
import model.Director;
import model.DirectorDAO;
import model.Film;
import model.FilmDAO;

/**
 * Servlet implementation class TestIndex
 */
@WebServlet({"/Actors", "/Actors/new", "/Actors/insert", "/Actors/edit", "/Actors/update", "/Actors/delete", "/Actors/list"})
public class Actors extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Actors() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        String action = request.getServletPath();
        
        try {
            switch (action) {
            case "/Actors/new":
                showNewForm(request, response);
                break;
            case "/Actors/insert":
                insertActor(request, response);
                break;
            case "/Actors/delete":
                deleteActor(request, response);
                break;
            case "/Actors/edit":
                showEditForm(request, response);
                break;
            case "/Actors/update":
                updateActor(request, response);
                break;
            default:
                listActors(request, response);
                break;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            throw new ServletException(ex);
        }
	}

	private void listActors(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, ServletException, ClassNotFoundException {
        List<Actor> actors = ActorDAO.listActors();
        request.setAttribute("actors", actors);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Actors/Actors.jsp");
        dispatcher.forward(request, response);
	}

	private void updateActor(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		
		Actor a = new Actor();
		a.setId(id);
		a.setName(name);
		a.setSurname(surname);
		
		ActorDAO.updateActor(a);
		
		response.sendRedirect("/SGTfinalTask/Actors");
	}

	private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException, ClassNotFoundException {
		int id = Integer.parseInt(request.getParameter("id"));
		Actor actor = ActorDAO.getActorById(id);
		request.setAttribute("actor", actor);
		RequestDispatcher dispatcher = request.getRequestDispatcher("ActorForm.jsp");
		dispatcher.forward(request, response);				
	}

	private void deleteActor(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException, ServletException {
		String idstr = request.getParameter("id");
		int id = Integer.parseInt(idstr);
		
		List<Film> films = FilmDAO.getFilmsByActorId(id);
		
		if (films.size() == 0) {	
			ActorDAO.deleteActorById(id);
			
			response.sendRedirect("/SGTfinalTask/Actors");
		} else {
	        request.setAttribute("actors", ActorDAO.listActors());
	        request.setAttribute("validationError", "This actor has films. Delete them first.");
	        RequestDispatcher dispatcher = request.getRequestDispatcher("/Actors/Actors.jsp");
	        dispatcher.forward(request, response);
		}
		
	}

	private void insertActor(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException {
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		
		Actor a = new Actor();
		a.setName(name);
		a.setSurname(surname);
		
		ActorDAO.createActor(a);
		
		response.sendRedirect("/SGTfinalTask/Actors");		
	}

	private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("ActorForm.jsp");
		dispatcher.forward(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
