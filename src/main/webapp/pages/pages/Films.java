package pages;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Actor;
import model.ActorDAO;
import model.DirectorDAO;
import model.Film;
import model.FilmDAO;

/**
 * Servlet implementation class TestIndex
 */
@WebServlet({"/Films", "/Films/new", "/Films/insert", "/Films/edit", "/Films/update", "/Films/delete", "/Films/list", "/Films/addactor", "/Films/removeactor", "/Films/actors"})
public class Films extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Films() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        String action = request.getServletPath();
        
        try {
            switch (action) {
            case "/Films/new":
                showNewForm(request, response);
                break;
            case "/Films/insert":
                insertFilm(request, response);
                break;
            case "/Films/delete":
                deleteFilm(request, response);
                break;
            case "/Films/edit":
                showEditForm(request, response);
                break;
            case "/Films/update":
                updateFilm(request, response);
                break;
            case "/Films/actors":
            	showActorsForm(request, response);
            	break;
            case "/Films/addactor":
            	addFilmActor(request, response);
            	break;
            case "/Films/removeactor":
            	removeFilmActor(request, response);
            	break;
            default:
                listFilms(request, response);
                break;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            throw new ServletException(ex);
        }
	}

	private void showActorsForm(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Film film = FilmDAO.getFilmById(id);
		List<Actor> allActors = ActorDAO.listActors();
		ArrayList<Actor> availableActors = new ArrayList<Actor>();
		
		for(Actor actor : allActors) {
			if (!film.getActors().contains(actor)) {
				availableActors.add(actor);
			}
		}
		
		request.setAttribute("film", film);
		request.setAttribute("availableActors", availableActors);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Films/FilmActorForm.jsp");
		dispatcher.forward(request, response);
	}

	private void removeFilmActor(HttpServletRequest request, HttpServletResponse response) throws ClassNotFoundException, SQLException, ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		int actor_id = Integer.parseInt(request.getParameter("actor_id"));
		
		FilmDAO.removeActor(id, actor_id);
		
		response.sendRedirect("/SGTfinalTask/Films/actors?id=" + Integer.toString(id));
	}

	private void addFilmActor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException {
		int id = Integer.parseInt(request.getParameter("id"));
		int actor_id = Integer.parseInt(request.getParameter("actor_id"));
		
		FilmDAO.addActor(id, actor_id);
		
		response.sendRedirect("/SGTfinalTask/Films/actors?id=" + Integer.toString(id));
	}

	private void listFilms(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, ServletException, ClassNotFoundException {
        List<Film> films = FilmDAO.listFilms();
        request.setAttribute("films", films);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Films/Films.jsp");
        dispatcher.forward(request, response);
	}

	private void updateFilm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			String title = request.getParameter("title");
			int director_id = Integer.parseInt(request.getParameter("director_id"));
			int year = Integer.parseInt(request.getParameter("year"));
			
			Film a = new Film();
			a.setId(id);
			a.setTitle(title);
			a.setDirectorId(director_id);
			a.setYear(year);;
			
			FilmDAO.updateFilm(a);
		} catch (NumberFormatException ex) {
			//TODO: do proper validation message
		}
		
		response.sendRedirect("/SGTfinalTask/Films");
	}

	private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException, ClassNotFoundException {
		int id = Integer.parseInt(request.getParameter("id"));
		Film film = FilmDAO.getFilmById(id);
		request.setAttribute("film", film);
		request.setAttribute("directors", DirectorDAO.listDirectors());
		RequestDispatcher dispatcher = request.getRequestDispatcher("FilmForm.jsp");
		dispatcher.forward(request, response);				
	}

	private void deleteFilm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException {
		String idstr = request.getParameter("id");
		int id = Integer.parseInt(idstr);
		
		FilmDAO.deleteFilmById(id);
		
		response.sendRedirect("/SGTfinalTask/Films");
	}

	private void insertFilm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException {
		try {
			String title = request.getParameter("title");
			int director_id = Integer.parseInt(request.getParameter("director_id"));
			int year = Integer.parseInt(request.getParameter("year"));
			
			Film a = new Film();
			a.setTitle(title);
			a.setDirectorId(director_id);
			a.setYear(year);
			
			FilmDAO.createFilm(a);
		} catch (NumberFormatException ex) {
			//TODO: do proper validation message
		}
		
		response.sendRedirect("/SGTfinalTask/Films");		
	}

	private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("FilmForm.jsp");
		request.setAttribute("directors", DirectorDAO.listDirectors());
		dispatcher.forward(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
