package pages;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Director;
import model.DirectorDAO;
import model.Film;
import model.FilmDAO;

/**
 * Servlet implementation class TestIndex
 */
@WebServlet({"/Directors", "/Directors/new", "/Directors/insert", "/Directors/edit", "/Directors/update", "/Directors/delete", "/Directors/list"})
public class Directors extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Directors() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        String action = request.getServletPath();
        
        try {
            switch (action) {
            case "/Directors/new":
                showNewForm(request, response);
                break;
            case "/Directors/insert":
                insertDirector(request, response);
                break;
            case "/Directors/delete":
                deleteDirector(request, response);
                break;
            case "/Directors/edit":
                showEditForm(request, response);
                break;
            case "/Directors/update":
                updateDirector(request, response);
                break;
            default:
                listDirectors(request, response);
                break;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            throw new ServletException(ex);
        }
	}

	private void listDirectors(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException, ServletException, ClassNotFoundException {
        List<Director> directors = DirectorDAO.listDirectors();
        request.setAttribute("directors", directors);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Directors/Directors.jsp");
        dispatcher.forward(request, response);
	}

	private void updateDirector(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException {
		// TODO Auto-generated method stub
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		
		Director a = new Director();
		a.setId(id);
		a.setName(name);
		a.setSurname(surname);
		
		DirectorDAO.updateDirector(a);
		
		response.sendRedirect("/SGTfinalTask/Directors");
	}

	private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException, ClassNotFoundException {
		int id = Integer.parseInt(request.getParameter("id"));
		Director director = DirectorDAO.getDirectorById(id);
		request.setAttribute("director", director);
		RequestDispatcher dispatcher = request.getRequestDispatcher("DirectorForm.jsp");
		dispatcher.forward(request, response);				
	}

	private void deleteDirector(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException, ServletException {
		String idstr = request.getParameter("id");
		int id = Integer.parseInt(idstr);
		
		List<Film> directorFilms = FilmDAO.getFilmsByDirectorId(id);
		if (directorFilms.size() == 0) {
			DirectorDAO.deleteDirectorById(id);
		
			response.sendRedirect("/SGTfinalTask/Directors");
		} else {
	        List<Director> directors = DirectorDAO.listDirectors();
	        request.setAttribute("directors", directors);
	        request.setAttribute("validationError", "This director has films. Delete them first.");
	        RequestDispatcher dispatcher = request.getRequestDispatcher("/Directors/Directors.jsp");
	        dispatcher.forward(request, response);
		}
	}

	private void insertDirector(HttpServletRequest request, HttpServletResponse response) throws SQLException, ClassNotFoundException, IOException {
		String name = request.getParameter("name");
		String surname = request.getParameter("surname");
		
		Director a = new Director();
		a.setName(name);
		a.setSurname(surname);
		
		DirectorDAO.createDirector(a);
		
		response.sendRedirect("/SGTfinalTask/Directors");		
	}

	private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("DirectorForm.jsp");
		dispatcher.forward(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
