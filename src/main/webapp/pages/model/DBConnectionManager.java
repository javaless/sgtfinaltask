package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionManager {
	private static Connection con = null;
	
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		if (DBConnectionManager.con == null) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			DBConnectionManager.con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/movies", "root", "sonata11");
			DBConnectionManager.con.setAutoCommit(false);
		}
		
		return DBConnectionManager.con;
	}

}
