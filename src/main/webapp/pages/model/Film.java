package model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Film {
	private int id;
	private String title;
	private int director_id = -1;
	private int year;
	private Director director;
	private List<Actor> actors;
	
	public int getId() {
		return this.id;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getYear() {
		return this.year;
	}
	
	public Director getDirector() throws SQLException, ClassNotFoundException {
		if (this.director == null & this.director_id != -1) {
			this.director = DirectorDAO.getDirectorById(this.director_id);
		}
		return this.director;
	}
	
	public List<Actor> getActors() throws SQLException, ClassNotFoundException {
		if (this.actors == null) {
			this.actors = ActorDAO.getActorsInFilm(this.id);
		}
		return this.actors;
	}
	
	public String getActorNames() throws ClassNotFoundException, SQLException {
		StringJoiner sb = new StringJoiner(", ");
		for (Actor a : this.getActors()) {
			sb.add(a.getFullName());
		}
		return sb.toString();
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public void setDirector(Director director) {
		this.director = director;
		this.director_id = director.getId();
	}
	
	public void setDirectorId(int director_id) {
		this.director = null;
		this.director_id = director_id;
	}

}
