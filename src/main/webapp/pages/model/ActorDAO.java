package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;

public class ActorDAO {
	public static Actor createActor(Actor actor) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement stmt = con.prepareStatement("INSERT INTO actors (name, surname) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, actor.getName());
		stmt.setString(2, actor.getSurname());
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Creating actor failed, no rows affected.");
        }

        try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                actor.setId(generatedKeys.getInt(1));
            }
            else {
                throw new SQLException("Creating actor failed, no ID obtained.");
            }
        }
        
		con.commit();
		return actor;
	}
	
	public static Actor updateActor(Actor actor) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement stmt = con.prepareStatement("UPDATE actors SET name = ?, surname = ? WHERE id = ?");
		stmt.setString(1, actor.getName());
		stmt.setString(2, actor.getSurname());
		stmt.setInt(3,  actor.getId());
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Updating actor failed, no rows affected.");
        }
        
		con.commit();
		return actor;	
	}
	
	public static void deleteActorById(int id) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement stmt = con.prepareStatement("DELETE FROM actors WHERE id = ?");
		stmt.setInt(1,  id);
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Deleting actor failed, no rows affected.");
        }
        
		con.commit();
	}
	
	public static List<Actor> listActors(int pageNum, int pageSize) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT * FROM actors LIMIT ? OFFSET ?");
		stmt.setInt(1, pageSize);
		stmt.setInt(2, pageNum * pageSize);
		
		List<Actor> actors = new ArrayList<Actor>();
		
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            actors.add(ActorDAO.actorFromRS(rs));
        }
		
		return actors;
	}
	
	public static Actor getActorById(int id) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT * FROM actors WHERE id = ?");
		stmt.setInt(1, id);
		
		ResultSet rs = stmt.executeQuery();
		
		if (!rs.next()){
			throw new SQLException("No actor with this id");
		}
		
		return ActorDAO.actorFromRS(rs);
	}
	
	public static List<Actor> listActors() throws SQLException, ClassNotFoundException {
		return ActorDAO.listActors(0, 1000);
	}
	
	private static Actor actorFromRS(ResultSet rs) throws SQLException {
		Actor a = new Actor();
		a.setId(rs.getInt("id"));
		a.setName(rs.getString("name"));
		a.setSurname(rs.getString("surname"));
		return a;
	}

	public static List<Actor> getActorsInFilm(int id) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT actors.* FROM actors inner join actorsinfilms as a on actors.id = a.actor_id WHERE a.film_id = ?");
		stmt.setInt(1,  id);
		List<Actor> actors = new ArrayList<Actor>();
		
		ResultSet rs = stmt.executeQuery();
		
		while (rs.next()) {
			actors.add(ActorDAO.actorFromRS(rs));
		}
		
		return actors;
	}

}
