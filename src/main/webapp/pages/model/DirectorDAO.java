package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;

public class DirectorDAO {
	public static Director createDirector(Director director) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement stmt = con.prepareStatement("INSERT INTO directors (name, surname) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, director.getName());
		stmt.setString(2, director.getSurname());
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Creating director failed, no rows affected.");
        }

        try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                director.setId(generatedKeys.getInt(1));
            }
            else {
                throw new SQLException("Creating director failed, no ID obtained.");
            }
        }
        
		con.commit();
		return director;
	}
	
	public static Director updateDirector(Director director) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement stmt = con.prepareStatement("UPDATE directors SET name = ?, surname = ? WHERE id = ?");
		stmt.setString(1, director.getName());
		stmt.setString(2, director.getSurname());
		stmt.setInt(3,  director.getId());
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Updating director failed, no rows affected.");
        }
        
		con.commit();
		return director;	
	}
	
	public static void deleteDirectorById(int id) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement stmt = con.prepareStatement("DELETE FROM directors WHERE id = ?");
		stmt.setInt(1,  id);
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Deleting director failed, no rows affected.");
        }
        
		con.commit();
	}
	
	public static List<Director> listDirectors(int pageNum, int pageSize) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT * FROM directors LIMIT ? OFFSET ?");
		stmt.setInt(1, pageSize);
		stmt.setInt(2, pageNum * pageSize);
		
		List<Director> directors = new ArrayList<Director>();
		
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            Director a = new Director();
            a.setId(rs.getInt("id"));
            a.setName(rs.getString("name"));
            a.setSurname(rs.getString("surname"));
            directors.add(a);
        }
		
		return directors;
	}
	
	public static Director getDirectorById(int id) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT * FROM directors WHERE id = ?");
		stmt.setInt(1, id);
		
		ResultSet rs = stmt.executeQuery();
		
		if (!rs.next()){
			throw new SQLException("No director with this id");
		}
		
		Director a = new Director();
		a.setId(rs.getInt("id"));
		a.setName(rs.getString("name"));
		a.setSurname(rs.getString("surname"));
		
		return a;
	}
	
	public static List<Director> listDirectors() throws SQLException, ClassNotFoundException {
		return DirectorDAO.listDirectors(0,1000);
	}

}
