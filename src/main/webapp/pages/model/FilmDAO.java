package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;

public class FilmDAO {
	public static Film createFilm(Film film) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement stmt = con.prepareStatement("INSERT INTO films (title, director_id, year) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, film.getTitle());
		stmt.setInt(2, film.getDirector().getId());
		stmt.setInt(3, film.getYear());
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Creating film failed, no rows affected.");
        }

        try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                film.setId(generatedKeys.getInt(1));
            }
            else {
                throw new SQLException("Creating film failed, no ID obtained.");
            }
        }
        
		con.commit();
		return film;
	}
	
	public static Film updateFilm(Film film) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement stmt = con.prepareStatement("UPDATE films SET title = ?, director_id = ?, year = ? WHERE id = ?");
		stmt.setString(1, film.getTitle());
		stmt.setInt(2, film.getDirector().getId());
		stmt.setInt(3, film.getYear());
		stmt.setInt(4,  film.getId());
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Updating film failed, no rows affected.");
        }
        
		con.commit();
		return film;	
	}
	
	public static void deleteFilmById(int id) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		
		PreparedStatement delete_links = con.prepareStatement("DELETE FROM actorsinfilms WHERE film_id = ?");
		delete_links.setInt(1, id);
		delete_links.execute();
		
		PreparedStatement stmt = con.prepareStatement("DELETE FROM films WHERE id = ?");
		stmt.setInt(1,  id);
		int affectedRows = stmt.executeUpdate();
		
        if (affectedRows == 0) {
            throw new SQLException("Deleting film failed, no rows affected.");
        }
        
		con.commit();
	}
	
	public static void removeActor(int id, int actor_id) throws ClassNotFoundException, SQLException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement delete = con.prepareStatement("DELETE FROM actorsinfilms WHERE film_id = ? and actor_id = ?");
		delete.setInt(1, id);
		delete.setInt(2, actor_id);
		
		delete.execute();
		
		con.commit();
	}
	
	public static void addActor(int id, int actor_id) throws ClassNotFoundException, SQLException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement insert = con.prepareStatement("INSERT INTO actorsinfilms(film_id, actor_id) VALUES(?, ?)");
		insert.setInt(1,  id);
		insert.setInt(2, actor_id);
		
		insert.execute();
		
		con.commit();
	}
	
	public static Film filmFromRS(ResultSet rs) throws SQLException {
        Film a = new Film();
        a.setId(rs.getInt("id"));
        a.setTitle(rs.getString("title"));
        a.setDirectorId(rs.getInt("director_id"));
        a.setYear(rs.getInt("year"));
        return a;
	}
	
	public static List<Film> listFilms(int pageNum, int pageSize) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT * FROM films LIMIT ? OFFSET ?");
		stmt.setInt(1, pageSize);
		stmt.setInt(2, pageNum * pageSize);
		
		List<Film> films = new ArrayList<Film>();
		
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            films.add(filmFromRS(rs));
        }
		
		return films;
	}
	
	public static Film getFilmById(int id) throws SQLException, ClassNotFoundException {
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT * FROM films WHERE id = ?");
		stmt.setInt(1, id);
		
		ResultSet rs = stmt.executeQuery();
		
		if (!rs.next()){
			throw new SQLException("No films with this id");
		}
		
		return filmFromRS(rs);
	}
	
	public static List<Film> getFilmsByDirectorId(int id) throws SQLException, ClassNotFoundException{
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT * FROM films WHERE director_id = ?");
		stmt.setInt(1, id);
		List<Film> films = new ArrayList<Film>();
		
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            films.add(filmFromRS(rs));
        }
		
		return films;
	}
	
	public static List<Film> getFilmsByActorId(int id) throws ClassNotFoundException, SQLException{
		Connection con = DBConnectionManager.getConnection();
		PreparedStatement stmt = con.prepareStatement("SELECT f.* FROM films as f inner join actorsinfilms as a on f.id = a.film_id WHERE a.actor_id = ?");
		stmt.setInt(1, id);
		List<Film> films = new ArrayList<Film>();
		
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            films.add(filmFromRS(rs));
        }
		
		return films;
	}
	
	public static List<Film> listFilms() throws SQLException, ClassNotFoundException {
		return FilmDAO.listFilms(0, 1000);
	}

}
