<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="Images/Group 2.png">
<style>
<%@ include file="style.css" %>
</style>


<title>index</title>
</head>
<body>
<div class="header">
        <h1 id="home" align="center"> Catalogue of Latvian Films </h1>
  </div>
  <%@ include file="navbar.html" %>  <br>
  <div class="gallery">
  <a target="_blank" href="Images/namejs.jpg">
    <img src="Images/namejs.jpg" alt="namejs" width="700" height="600">
  </a>
  <div class="desc">Historical drama</div>
</div>

<div class="gallery">
  <a target="_blank" href="Images/dveseluputenis.jpg">
    <img src="Images/dveseluputenis.jpg" alt="putenis" width="700" height="600">
  </a>
  <div class="desc">Programm for May Film Marathon</div>
</div>
  
</body>
</html>