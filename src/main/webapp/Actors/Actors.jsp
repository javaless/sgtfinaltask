<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="Images/Group 2.png">
<style>
<%@ include file="../style.css" %>
</style>


<title>films</title>
</head>
<body>
<div class="header">
        <h1 id="artists" align="center"> Artists </h1>
  </div>
  <%@ include file="../navbar.html" %>


  <div class="main">
    <h2>List of Actors</h2>
    <p>You could add, rename and delete actors here.</p>
    <a href="/SGTfinalTask/Actors/new">Add new actor</a>
  </div>
    <div align="center">
        <table border="1">
            <caption><h2>List of Actors</h2></caption>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="actor" items="${actors}">
                <tr>
                    <td><c:out value="${actor.getId()}" /></td>
                    <td><c:out value="${actor.getName()}" /></td>
                    <td><c:out value="${actor.getSurname()}" /></td>
                    <td>
                        <a href="/SGTfinalTask/Actors/edit?id=<c:out value='${actor.getId()}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="/SGTfinalTask/Actors/delete?id=<c:out value='${actor.getId()}' />">Delete</a>                     
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div> 
    <p style="color:​red;"><c:out value='${validationError}' /></p>
  
</body>
</html>