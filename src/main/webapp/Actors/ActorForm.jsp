<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="Images/Group 2.png">
<style>
<%@ include file="../style.css" %>
</style>


<title>Actors</title>
</head>
<body>
<div class="header">
	<h1>
	    <c:if test="${actor != null}">
	        Edit Actor
	    </c:if>
	    <c:if test="${actor == null}">
	        Add New Actor
	    </c:if>
	</h1>
</div>
  <%@ include file="../navbar.html" %>


  <div class="main">
        <c:if test="${actor != null}">
            <form action="update" method="post">
        </c:if>
        <c:if test="${actor == null}">
            <form action="insert" method="post">
        </c:if>
      <c:if test="${actor != null}">
          <input type="hidden" name="id" value="<c:out value='${actor.getId()}' />" />
      </c:if>    
	  <label for="name">Actor name:</label><br><input type="text" id="name" name="name" size="100" value="<c:out value='${actor.getName()}' />" ><br><br>
	  <label for="surname">Actor surname:</label><br><input type="text" id="surname" name="surname" size="100" value="<c:out value='${actor.getSurname()}' />" ><br><br>
	 	<c:if test="${actor != null}">
            <input type="submit" value="Update actor" > 
        </c:if>
        <c:if test="${actor == null}">
            <input type="submit" value="Add actor" > 
        </c:if>
	  
	  <a href="list">Cancel</a>
	  
  	</form>
  </div>
  
</body>
</html>