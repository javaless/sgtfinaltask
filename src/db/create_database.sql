CREATE SCHEMA IF NOT EXISTS movies;

DROP TABLE IF EXISTS movies.actorsinfilms;
;
DROP TABLE IF EXISTS movies.films;
DROP TABLE IF EXISTS movies.directors;
CREATE TABLE movies.directors(
	id int primary key AUTO_INCREMENT,
    name varchar(100),
    surname varchar(100)
);

DROP TABLE IF EXISTS movies.actors;
CREATE TABLE movies.actors(
	id int primary key AUTO_INCREMENT, 
    name varchar(100),
    surname varchar(100)
);


CREATE TABLE movies.films(
	id int primary key AUTO_INCREMENT,
    title varchar(100),
    director_id int,
    year int,
    foreign key (director_id) REFERENCES movies.directors(id)
);

CREATE TABLE movies.actorsinfilms(
	actor_id int,
    film_id int,
    foreign key (actor_id) REFERENCES movies.actors(id),
    foreign key (film_id) REFERENCES movies.films(id)
);
